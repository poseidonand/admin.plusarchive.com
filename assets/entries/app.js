// js
window.$ = window.jQuery = require('jquery')

require('bootstrap/js/dist/util')
require('bootstrap/js/dist/button')
require('bootstrap/js/dist/collapse')
require('bootstrap/js/dist/dropdown')
require('bootstrap/js/dist/modal')

require('../../vendor/yiisoft/yii2/assets/yii')
require('../../vendor/yiisoft/yii2/assets/yii.activeForm')
require('../../vendor/yiisoft/yii2/assets/yii.gridView')
require('../../vendor/yiisoft/yii2/assets/yii.validation')
require('yii2-pjax/jquery.pjax')

require('masonry-layout/dist/masonry.pkgd')
require('lazysizes/lazysizes')

require('select2/dist/js/select2')

window.toastr = require('toastr/toastr')

// images
require('../images/favicon.png')
require('../images/apple-touch-icon.png')

// css
require('../css/app.scss')
